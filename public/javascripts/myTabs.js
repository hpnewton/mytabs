var myApp = angular.module('myTabs', []);

myApp.controller('tabController',  function(){

  this.inputVal = '...';

  this.tab = 1;

  this.isSet = function(checkTab) {
    return this.tab === checkTab;
  };

  this.setTab = function(activeTab) {
    this.tab = activeTab;
  };

});

angular.module('myTabs')
.directive('tabDirective', function() {
    return {
      restrict: 'E',
      templateUrl: './templates/tab-directive.html',
      controller:  ["$http", "$scope", function($http, $scope) {
        this.tab = 1;

        this.isSet = function(checkTab) {
          return this.tab === checkTab;
        };

        this.setTab = function(activeTab) {
          this.tab = activeTab;
        };

        //debugger;

        var tabData = this;
        $http({method: 'GET', url:'/tabs.json'}).success(function(data){
          console.log(data);
          tabData.panes = data;
        });

      }],
      controllerAs: 'tab'
    };
  });

  angular.module('myTabs')
  .directive('tabSet', function() {
      return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: {},
        templateUrl: './templates/tab-set.html',
        // controller: function() {
        //   this.tab = 1;
        //
        //   this.isSet = function(checkTab) {
        //     return this.tab === checkTab;
        //   };
        //
        //   this.setTab = function(activeTab) {
        //     this.tab = activeTab;
        //   };
        // },
        controller: ["$scope", function($scope) {
          var tabs = $scope.tabs = [];

          $scope.setTab = function(tab) {
            angular.forEach(tabs, function(tab) {
              tab.isSet = false;
            });
            tab.isSet = true;
          };

          this.pushTab = function(tab) {
            if (tabs.length === 0)
              $scope.setTab(tab);
            tabs.push(tab);
          };
        }],
        controllerAs: 'tabSetCtl'
      };
    });


    angular.module('myTabs')
      .directive('tab', function() {
        return {
          require: '^tabSet',
          restrict: 'E',
          transclude: true,
          scope: {
            heading: '@'
          },
          link: function(scope, element, attrs, tabSetCtl) {
            tabSetCtl.pushTab(scope);
            console.log(tabSetCtl);
          },
          templateUrl: './templates/tab.html',
          replace: true
        };
      });



      angular.module('myTabs')
      .directive('tabJson', function() {
          return {
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {},
            templateUrl: './templates/tab-json.html',
            controller: ["$scope", "$http", function($scope, $http) {



              $http({method: 'GET', url:'/tabs.json'}).success(function(data){
                //console.log(data);
                $scope.panes = data;
              });


              $scope.pane = 1;

              $scope.isSet = function(checkTab) {
                return   $scope.pane === checkTab;
              };

              $scope.setTab = function(activeTab) {
                $scope.pane = activeTab;
              };

            }],
            controllerAs: 'tabJsonCtl'
          };
        });
